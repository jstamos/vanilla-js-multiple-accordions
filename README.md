# Vanilla JavaScript - Multiple Use Acccordions

Accordions for multiple use on a single page. There are two options: individual accordion toggle (allows for multiple open at one time) or the default toggle (click one, close another).

You will find, underneath the public folder, examples and the distribution code to use. If you'd like to look into the code as it was created, feel free to check out the src folder.

The intended use for these accordion scripts are on the JcInk forums for RPGs or similar RPG sites. Please maintain credit. I'd also love for you to share with me anything and everything that you create! I enjoy seeing everyone's work. :) If you have any questions, feel free to contact me at jessica@jessicastamos.com.