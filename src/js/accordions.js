/**
* Created by Jessica Stamos (https://jessicastamos.com)
* Refer to the repo for updates/additional information
* https://gitlab.com/jnstamos/vanilla-js-multiple-accordions
* Contact: jessica@jessicastamos.com
*/

const jsAccordions = (() => {
  const makeArray = (element) => Array.prototype.slice.call(element);
  const hasClass = (element, className) => { return (` ${element.className} `).indexOf(` ${className} `) > -1; };

  const getHeight = (element) => {
    element.style.display = 'block';
    const height = element.scrollHeight;
    element.style.display = '';
    return height;
  };

  const accordionAddHeight = (accordion) => {
    const accordionContent = accordion.nextElementSibling;
    const height = getHeight(accordionContent);

    if (hasClass(accordion, 'is-open')) {
      accordionContent.style.height = `${height}px`;
    } else {
      accordionContent.style.maxHeight = `${height}px`;
    }
  };

  const toggleAccordion = accordion => {
    const clicked = accordion.target;
    const accordionParent = clicked.parentNode;
    const accordionToggle = accordionParent.getAttribute('data-toggle');
    const activeAccordion = accordionParent.querySelector('.is-open');
    const accordionContent = clicked.nextElementSibling;

    switch(accordionToggle) {
      case 'individual':
        if (hasClass(clicked, 'is-open')) {
          accordionContent.style.height = 0;
          clicked.className = 'accordion__header';
        } else {
          const height = getHeight(accordionContent);

          clicked.className = 'accordion__header is-open';
          accordionContent.style.height = `${height}px`;
        }
        break;
      default:
        const activeAccordionContent = activeAccordion.nextElementSibling;
        if (!hasClass(clicked, 'is-open')) {
          const height = getHeight(accordionContent);

          activeAccordion.className = 'accordion__header';
          activeAccordionContent.className = activeAccordionContent.className.replace('is-open', '');
          activeAccordionContent.style.height = 0;
          accordionContent.style.height = 0;
          clicked.className = 'accordion__header is-open';
          accordionContent.style.height = `${height}px`;
        }
        break;
    }
  };

  const accordionsArray = makeArray(document.querySelectorAll('.accordion__header'));

  accordionsArray.forEach(accordion => {
    accordionAddHeight(accordion);
    accordion.addEventListener('click', toggleAccordion);
  });
})();