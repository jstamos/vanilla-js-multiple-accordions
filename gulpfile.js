var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    sourcemaps    = require('gulp-sourcemaps'),
    babel         = require('gulp-babel'),
    autoprefixer  = require('gulp-autoprefixer'),
    plumber       = require('gulp-plumber'),
    rename        = require('gulp-rename'),
    uglify        = require('gulp-uglify');

function javascript () {
  return gulp.src('src/js/**/*.js')
      .pipe(plumber())
      .pipe(babel({ presets: ['@babel/preset-env'] }))
      .pipe(sourcemaps.init())
      .pipe(uglify({
        mangle: false
      }))
      .pipe(rename({ suffix: '.min' }))
      .pipe(plumber.stop())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('public/dist/js'));
}

function scss () {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('public/dist/css'));
}

function watch () {
  javascript();
  scss();

  gulp.watch('src/js/**/*.js', javascript);
  gulp.watch('src/scss/**/*.scss', scss);
}

exports.watch = watch;